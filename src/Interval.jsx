import React, { useState } from "react";

function Interval() {
    const [times, setTimes] = useState([]);

    const setDate = () => {
        setTimes(prevTimes => [...prevTimes, Date.now()]);
    };

   

    const da = (date) => {
        let h = date.getHours();
        let m = date.getMinutes();
        let s = date.getSeconds();
        let ms = date.getMilliseconds();

        return h + ':' + m + ':' + s + '-' + ms;
    }

    return (
        <div>
            <p>w41</p>
            <button onClick={setDate}>Interval time</button>            
            {times.map((time, index) => (
                <p key={index}>{da(new Date(time))}</p>
            ))}
            
        </div>
    )
}  
export default Interval;